# --- Initial Schema

# --- !Ups
CREATE SEQUENCE driver_id_seq;

CREATE TABLE driver (
  driverId          NUMERIC DEFAULT driver_id_seq.nextval PRIMARY KEY,
  driverAvailable   BOOLEAN,
  latitude          NUMERIC,
  longitude         NUMERIC,
  name              VARCHAR,
  carPlate          VARCHAR(8)
)


# --- !Downs
CREATE SEQUENCE IF EXISTS driver_id_seq;
DROP TABLE IF EXISTS driver