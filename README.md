# Projeto Backend Taxis - Case 99
## Endpoints


```
#!
GET     /drivers/:driverId/status   controllers.DriverController.status(driverId: Long)
POST    /drivers/:driverId/status   controllers.DriverController.updateStatus(driverId: Long)
GET     /drivers/inArea             controllers.DriverController.inArea
POST    /drivers                    controllers.DriverController.create
GET     /drivers/new                controllers.DriverController.newDriver


```

## Link na nuvem
### http://app-case99.tarla.com.br

## Repositório Git (BitBucket)
### https://bitbucket.org/joaotarla/case99 (Público)