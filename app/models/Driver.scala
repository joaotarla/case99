package models

import anorm.SqlParser._
import anorm._
import play.api.Logger
import play.api.Play.current
import play.api.db.DB
import play.api.libs.json.Json

/**
 * Created by tarla on 4/13/15.
 */
case class Driver(driverId: Long, driverAvailable: Boolean, latitude: Double, longitude: Double, name: String, carPlate: String)

object Driver {
  val simple = {
    get[Long]("driverId") ~
      get[Boolean]("driverAvailable") ~
      get[Double]("latitude") ~
      get[Double]("longitude") ~
      get[String]("name") ~
      get[String]("carPlate") map {
      case driverId ~ driverAvailable ~ latitude ~ longitude ~ name ~ carPlate => Driver(driverId, driverAvailable, latitude, longitude, name, carPlate)
    }
  }

  def create(driver: Driver): Long = {
    DB.withConnection { implicit connection =>
      SQL(
        """INSERT INTO driver (
             driverAvailable,
             latitude,
             longitude,
             name,
             carPlate
           )
           VALUES (
             {driverAvailable},
             {latitude},
             {longitude},
             {name},
             {carPlate}
           )
        """).on(
          'driverAvailable -> driver.driverAvailable,
          'latitude -> driver.latitude,
          'longitude -> driver.longitude,
          'name -> driver.name,
          'carPlate -> driver.carPlate
        ).executeInsert(scalar[Long] single)
    }
  }

  def findAll(): Seq[Driver] = {
    DB.withConnection { implicit connection =>
      SQL("SELECT * FROM driver").as(Driver.simple *)
    }
    //Seq(new Driver(1, true, -23.60810717, -46.67500346), new Driver(2, true, -23.60810717, -46.67500346), new Driver(3, true, -23.60810717, -46.67500346), new Driver(4, true, -23.60810717, -46.67500346))
  }

  def findByBoundaries(ne: Coordinate, sw: Coordinate): Seq[Driver] = {
    DB.withConnection { implicit connection =>
      //AND latitude BETWEEN {neLatitude} AND {swLatitude}
      //AND longitude BETWEEN {neLongitude} AND {swLongitude}
      SQL(
        """
        SELECT * FROM driver
         WHERE
         driverAvailable = TRUE
         AND latitude BETWEEN {swLatitude} AND {neLatitude}
         AND longitude BETWEEN {swLongitude} AND {neLongitude}
        """
      ).on(
          'neLatitude -> ne.latitude,
          'neLongitude -> ne.longitude,
          'swLatitude -> sw.latitude,
          'swLongitude -> sw.longitude
        ).as(Driver.simple *)
    }
    //Seq(new Driver(1, true, -23.60810717, -46.67500346), new Driver(2, true, -23.60810717, -46.67500346), new Driver(3, true, -23.60810717, -46.67500346), new Driver(4, true, -23.60810717, -46.67500346))
  }

  def update(driver: Driver): Int = {
    Logger.info("m=update, msg=updating driverId " + driver.driverId)
    DB.withConnection { implicit connection =>
      SQL(
        """
           UPDATE driver
           SET
             driverAvailable = {driverAvailable},
             latitude = {latitude},
             longitude = {longitude}
           WHERE
             driverId = {driverId}
        """).on(
          'driverId -> driver.driverId,
          'driverAvailable -> driver.driverAvailable,
          'latitude -> driver.latitude,
          'longitude -> driver.longitude
        ).executeUpdate()
    }
  }

  def find(driverId: Long): Option[Driver] = {
    Logger.info("m=find, msg=searching driverId" + driverId)
    DB.withConnection { implicit connection =>
      SQL(
        """
        SELECT * FROM driver
         WHERE
         driverId = {driverId}
        """
      ).on(
          'driverId -> driverId
        ).as(Driver.simple.singleOpt)
    }
  }

  val count: Int = {
    DB.withConnection { implicit c =>
      SQL("SELECT count(*) FROM driver").as(scalar[Int].single)
    }
  }

  implicit val driverFormat = Json.format[Driver]
}
