import models.Driver
import play.api._

/**
 * Created by tarla on 4/16/15.
 */
object Global extends GlobalSettings {
  override def onStart(app: Application): Unit = {
    InitialData.insert()
  }
}

object InitialData {
  val drivers = Seq(
    /*new Driver(0, true, 23.589548, -46.673392, "Joao", "ENB-5374"), //ne
    new Driver(0, true, -23.612474, -46.702746, "Pedro", "AAA-3333"),
    new Driver(0, true, 1, 1, "Fernando", "AAB-5334"),
    new Driver(0, true, 1, 1, "Francisco", "AAC-5335")*/
    new Driver(0, true, 44.837102050663795, -78.50342480468748, "Joao", "ENB-5374"), //ne
    new Driver(0, true, 44.48608119469483, -79.45924169921875, "Pedro", "AAA-3333"), //sw
    new Driver(0, true, 44.77669356095164, -78.73413769531248, "Fernando", "AAB-5334"), //sub ne
    new Driver(0, true, 44.56049347341024, -79.27796728515625, "Francisco", "AAC-5335") //sub sw
  )

  def insert(): Unit = {
    if (Driver.count == 0) drivers.foreach(Driver.create)

  }
}