package controllers

import models.{Coordinate, Driver}
import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._
import play.api.libs.json._
import play.api.mvc.{Action, Controller}

/**
 * Created by tarla on 4/13/15.
 */
object DriverController extends Controller {

  val createDriverForm: Form[Driver] = Form(
    mapping(
      "driverId" -> default(longNumber, 0l),
      "driverAvailable" -> default(boolean, false),
      "latitude" -> default(of(doubleFormat), 0.0),
      "longitude" -> default(of(doubleFormat), 0.0),
      "name" -> text,
      "carPlate" -> text(maxLength = 8)
    )(Driver.apply)(Driver.unapply)
  )

  val driverForm: Form[Driver] = Form(//Form default
    mapping(
      "driverId" -> default(longNumber, 0l),
      "driverAvailable" -> boolean,
      "latitude" -> of(doubleFormat),
      "longitude" -> of(doubleFormat),
      "name" -> default(text, ""),
      "carPlate" -> default(text, "")
    )(Driver.apply)(Driver.unapply)
  )

  def status(driverId: Long) = Action {
    implicit request =>
      val driver: Option[Driver] = Driver.find(driverId)
      if (!driver.isEmpty)
        Ok(Json.toJson(driver))
      else
        NotFound //404
  }

  def updateStatus(driverId: Long) = Action {
    implicit request =>
      driverForm.bindFromRequest.fold(
        hasError => {
          BadRequest(views.html.newDriver(hasError)) //500
        }
        , success => {
          val driver: Driver = success
          if (driverId == driver.driverId)
            if (Driver.update(driver) >= 1)
              Ok
            else
              NotFound // when id not exists
          else
            BadRequest("driverId should match URI parameters")
        }
      )
  }

  def inArea = Action { implicit request =>
    if (request.getQueryString("sw").isEmpty || request.getQueryString("ne").isEmpty) {
      BadRequest("Invalid parameters")
    } else {
      val swParam = request.getQueryString("sw").get.split(',')
      val neParam = request.getQueryString("ne").get.split(',')
      if (swParam.size < 2 || neParam.size < 2) {
        BadRequest("Invalid parameters")
      } else {
        val sw = new Coordinate(swParam(0).toDouble, swParam(1).toDouble)
        val ne = new Coordinate(neParam(0).toDouble, neParam(1).toDouble)
        Ok(Json.toJson(Driver.findByBoundaries(ne, sw)))
      }
    }
  }

  def create = Action { implicit request =>
    createDriverForm.bindFromRequest.fold(
      hasError => {
        BadRequest(views.html.newDriver(hasError))
      }
      , success => {
        val driver: Driver = createDriverForm.bindFromRequest.get
        val driverURI = "/drivers/" + Driver.create(driver)
        Created("Created").withHeaders("Location" -> driverURI)
      }
    )
  }

  def newDriver = Action {
    println(Driver.count)
    Ok(views.html.newDriver(driverForm))
  }
}