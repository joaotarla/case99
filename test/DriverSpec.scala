import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test.Helpers._
import play.api.test._
import play.api.libs.json.{Json, JsValue}

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class DriverSpec extends Specification {

  "Driver" should {
    "render the drivers search page" in new WithApplication {
      val search = route(FakeRequest(GET, "/drivers/inArea?sw=44.837102050663795,-78.50342480468748&ne=44.837102050663795, -78.50342480468748")).get

      status(search) must equalTo(OK)
      contentType(search) must beSome.which(_ == "application/json")
      contentAsString(search) must contain("driverId")
      contentAsString(search) must contain("driverAvailable")
      contentAsString(search) must contain("latitude")
      contentAsString(search) must contain("longitude")
    }

    "render the drivers search page with invalid parameters" in new WithApplication {
      val emptySearch = route(FakeRequest(GET, "/drivers/inArea?sw=9&ne=9,9")).get

      status(emptySearch) must equalTo(BAD_REQUEST)
    }

    "render the drivers search page when empty drivers" in new WithApplication {
      val emptySearch = route(FakeRequest(GET, "/drivers/inArea?sw=9,9&ne=9,9")).get

      status(emptySearch) must equalTo(OK)
      contentType(emptySearch) must beSome.which(_ == "application/json")
      contentAsString(emptySearch) must equalTo("[]")
    }

    "render the driver status page" in new WithApplication {
      val driverStatus = route(FakeRequest(GET, "/drivers/1/status")).get

      status(driverStatus) must equalTo(OK)
      contentType(driverStatus) must beSome.which(_ == "application/json")
      contentAsJson(driverStatus)
    }

    "render the not found status when driver id not exists" in new WithApplication {
      val driverNotFoundStatus = route(FakeRequest(GET, "/drivers/9999/status")).get

      status(driverNotFoundStatus) must equalTo(NOT_FOUND)
      contentAsString(driverNotFoundStatus) must beEmpty
    }

    "render the new driver page" in new WithApplication {
      val newDriver = route(FakeRequest(GET, "/drivers/new")).get

      status(newDriver) must equalTo(OK)
      contentType(newDriver) must beSome.which(_ == "text/html")
      contentAsString(newDriver) must contain("<h2>Adicionar taxista</h2>")
    }

    "update driver status" in new WithApplication {
      val driverId: Long = 1
      val json: JsValue = Json.parse(
        s"""{"driverId": $driverId,"driverAvailable" : false ,"latitude":1, "longitude": 2}"""
      )
      val updateDriverStatusOk = route(FakeRequest(POST, s"/drivers/$driverId/status").withJsonBody(json)).get
      status(updateDriverStatusOk) must equalTo(OK)
    }

    "not update driver status when id not exists" in new WithApplication {
      val driverId: Long = 9999
      val json: JsValue = Json.parse(
        s"""{"driverId": $driverId,"driverAvailable" : false ,"latitude":1, "longitude": 2}"""
      )
      val updateDriverStatusOk = route(FakeRequest(POST, s"/drivers/$driverId/status").withJsonBody(json)).get
      status(updateDriverStatusOk) must equalTo(NOT_FOUND)
    }

    "not update driver status when id not match request driverId" in new WithApplication {
      val driverId: Long = 1
      val json: JsValue = Json.parse(
        """{"driverId": 2,"driverAvailable" : false ,"latitude":1, "longitude": 2}"""
      )
      val updateDriverStatusOk = route(FakeRequest(POST, s"/drivers/$driverId/status").withJsonBody(json)).get
      status(updateDriverStatusOk) must equalTo(BAD_REQUEST)
    }

    "create new driver" in new WithApplication {
      val json: JsValue = Json.parse( """{"name" : "Pedro","carPlate" : "RPC-9999"}""")
      val newDriverCreated = route(FakeRequest(POST, "/drivers").withJsonBody(json)).get
      status(newDriverCreated) must equalTo(CREATED)
    }

    "create new driver missing parameter" in new WithApplication {
      val json: JsValue = Json.parse( """{"carPlate":"RPC-9999"}""")
      //missing parameter
      val newDriverMissingParameter = route(FakeRequest(POST, "/drivers").withJsonBody(json)).get
      status(newDriverMissingParameter) must equalTo(BAD_REQUEST)
    }
  }
}